import QuestionAnswer from './question-answer.js'

const template = document.createElement('template')

template.innerHTML = `
    <style>
     :host([hidden]) { display: none }
     :host {
	 box-sizing: border-box;
	 display: flex;
	 justify-content: center;
	 align-items: center;
	 min-height: 90vh;
	 max-width: 40rem;
	 width: 100%;
	 padding: 1rem;
	 position: relative;
     }
     .Index {}
     .Title {
	 font-style: italic;
	 font-size: 2rem;
     }
     .Component {
	 width: 100%;
     }
    .Content {
        font-size: 1.2rem;
	    line-height: 1.4;
	}
    </style>
    <div class="Component"></div>
`

const SurveyQuestion = class extends HTMLElement {
    constructor() {
	super()
	this.attachShadow({mode: 'open'})
	this.shadowRoot.appendChild(template.content.cloneNode(true))
    }
    async connectedCallback() {
	this.id = this.getAttribute('id')
	this.title = this.getAttribute('title')
	this.content = this.getAttribute('content')
	this.index = this.getAttribute('index')
	this.total = this.getAttribute('total')
	this.attachEvents()
	this.render()
    }
    attachEvents = () => {
	this.shadowRoot
	    .addEventListener('submitAnswer', this.handleSubmit, false)
    }
    handleSubmit = (event) => {
	this.answer = event.detail
    }
    render() {
	let $component = this.shadowRoot.querySelector('.Component')

	let index = document.createElement('p')
	index.classList.add('Index')
	index.innerHTML = `${this.index} / ${this.total}`
	$component.appendChild(index)

	let questionTitle = document.createElement('p')
	questionTitle.innerHTML = this.title
	questionTitle.classList.add('Title')
	$component.appendChild(questionTitle)

	let questionContent = document.createElement('p')
	questionContent.innerText = this.content
	questionContent.classList.add('Content')
	$component.appendChild(questionContent)

	let answer = document.createElement('question-answer')
	answer.innerHTML = this.title
	answer.classList.add('Title')
	$component.appendChild(answer)
    }
}

customElements.define('survey-question', SurveyQuestion)

export default SurveyQuestion
